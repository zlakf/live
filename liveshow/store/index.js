import Vue from 'vue'
import Vuex from 'vuex'
import * as TYPE from '@/store/mutation-type.js'

Vue.use(Vuex)

const states = {
	userInfo: {},
	hasLogin: false,    // 未登陆
	forcedLogin:false   // 未登陆
}

const getters = {
	
}

const mutations = {
	[TYPE.LIVE_LOGIN](state,data) {
		state.userInfo = data;
		state.hasLogin = state.userInfo ? true : false;
		state.forcedLogin = state.userInfo ? true : false;
	}
}

const actions = {
	
}

const store = new Vuex.Store({
	states,
	getters,
	mutations,
	actions
})
export default store
